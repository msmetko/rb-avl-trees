import hr.fer.zpr.nasp.AVLTree;
import hr.fer.zpr.nasp.BalancedTree;
import hr.fer.zpr.nasp.RedBlackTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        BalancedTree<Integer> tree = null;
        System.out.println("What type of tree you want to test?\n\t1\tAVL Tree\n\t2\tRed-Black Tree");
        int type;
        do {
            readUntilNumber(s);
            type = s.nextInt();
        } while ((type < 0) || (type > 2));
        switch (type) {
            case 1:
                tree = new AVLTree<>();
                break;
            case 2:
                tree = new RedBlackTree<>();
                break;
            default:
                System.exit(1);
                break;
        }
        System.out.println("How would you like to generate the tree?\n" +
                "\t1\tFrom a file (will read a list if integers from file)\n" +
                "\t2\tRandomize (will generate n random numbers from [0, 100]");
        int method;
        do {
            readUntilNumber(s);
            method = s.nextInt();
        } while ((method < 0) || (method > 2));
        switch (method) {
            case 1:
                System.out.print("\tWrite a filename: ");
                String filename = s.next();
                initializeFromFile(tree, filename);
                break;
            case 2:
                System.out.print("\tInput a number of nodes: ");
                int n = s.nextInt();
                initializeRandomTree(tree, n);
                break;
            default:
                break;
        }
        System.out.println("Your tree is loaded.\n" +
                "To insert a number enter 'i number'\n" +
                "To delete a number enter 'd number'\n" +
                "To exit, type 'q' or 'exit'");
        boolean more = true;
        while (more) {
            System.out.print("> ");
            String command = s.next();
            switch (command.toLowerCase()) {
                case "i":
                    readUntilNumber(s);
                    tree.insert(s.nextInt());
                    tree.print();
                    break;
                case "d":
                    readUntilNumber(s);
                    tree.delete(s.nextInt());
                    tree.print();
                    break;
                case ":q":
                case "q":
                case "exit":
                default:
                    more = false;
                    break;
            }
        }
    }

    private static void readUntilNumber(Scanner s) {
        while (!s.hasNextInt()) {
            System.out.println("Number not entered!");
            s.next();
        }
    }

    private static BalancedTree<Integer> initializeFromFile(BalancedTree<Integer> tree, String filename) {
        try {
            Scanner s = new Scanner(new File(filename));
            while (s.hasNext()) {
                if (s.hasNextInt()) {
                    tree.insert(s.nextInt());
                    tree.print();
                } else {
                    s.next();
                }
            }
            return tree;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    private static BalancedTree initializeRandomTree(BalancedTree<Integer> tree, int nodeCount) {
        Random r = new Random();
        for (int i = 0; i < nodeCount; i++) {
            tree.insert(r.nextInt(100));
            tree.print();
        }
        return tree;
    }
}
