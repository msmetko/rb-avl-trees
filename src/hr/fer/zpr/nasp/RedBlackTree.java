package hr.fer.zpr.nasp;

import lib.tech.vanyo.treePrinter.TreePrinter;

public class RedBlackTree<T extends Comparable<T>> implements BalancedTree<T> {
    private RBInternalNode root;
    private TreePrinter<RBInternalNode> printer = new TreePrinter<>(RBInternalNode::toString, n -> n.left, n -> n.right);

    @Override
    public BalancedTree insert(T value) {
        RBInternalNode newNode = new RBInternalNode(value);
        insert(newNode);
        return this;
    }

    private void insert(RBInternalNode newNode) {
        if (this.root == null) {
            this.root = newNode;
            this.root.color = Color.BLACK;
        } else {
            RBInternalNode root = this.root;
            RBInternalNode parent = root.parent;
            while (root != null) {
                parent = root;
                root = newNode.compareTo(root) <= 0 ? root.left : root.right;
            }
            newNode.parent = parent;
            if (newNode.compareTo(parent) <= 0) {
                parent.left = newNode;
            } else {
                parent.right = newNode;
            }
            insert_fixup(newNode);
        }
    }

    private void insert_fixup(RBInternalNode insertedNode) {
        RBInternalNode node = insertedNode;
        while (true) {
            if (node.parent == null) {
                node.color = Color.BLACK;
                return;
            } else if (node.parent.color == Color.BLACK) {
                // the black depth is the same on all paths
                return;
            } else {
                RBInternalNode parent = node.parent;
                RBInternalNode grandpa = node.getGrandparent();
                RBInternalNode uncle = node.getUncle();

                if (uncle != null && uncle.color == Color.RED) {
                    parent.color = Color.BLACK;
                    uncle.color = Color.BLACK;
                    grandpa.color = Color.RED;
                    node = grandpa;
                } else {
                    if (grandpa.left != null && node == grandpa.left.right) {
                        leftRotate(parent);
                        node = node.left;
                    } else if (grandpa.right != null && node == grandpa.right.left) {
                        rightRotate(parent);
                        node = node.right;
                    }
                    parent = node.parent;
                    grandpa = parent.parent;
                    if (node == parent.left) {
                        rightRotate(grandpa);
                    } else {
                        leftRotate(grandpa);
                    }
                    parent.color = Color.BLACK;
                    grandpa.color = Color.RED;
                }
            }
        }
    }

    private void rightRotate(RBInternalNode node) {
        RBInternalNode child = node.left;
        node.left = child.right;
        child.right = node;
        child.parent = node.parent;
        if (node.parent != null) {
            if (node.parent.left == node) {
                node.parent.left = child;
            } else {
                node.parent.right = child;
            }
        }
        if (node.left != null) {
            node.left.parent = node;
        }
        node.parent = child;
        if (child.parent == null) {
            this.root = child;
        }
    }

    private void leftRotate(RBInternalNode node) {
        RBInternalNode child = node.right;
        node.right = child.left;
        child.left = node;
        child.parent = node.parent;
        if (node.parent != null) {
            if (node.parent.left == node) {
                node.parent.left = child;
            } else {
                node.parent.right = child;
            }
        }
        if (node.right != null) {
            node.right.parent = node;
        }
        node.parent = child;

        if (child.parent == null) {
            this.root = child;
        }
    }

    @Override
    public BalancedTree delete(T value) {
        RBInternalNode root = this.root;
        while (root != null) {
            if (value.compareTo(root.value) == 0) {
                remove(root);
                break;
            } else if (value.compareTo(root.value) < 0) {
                root = root.left;
            } else {
                root = root.right;
            }
        }
        return this;
    }

    private void remove(RBInternalNode toDelete) {
        RBInternalNode node = toDelete;
        if (node.left != null && node.right != null) {
            node = toDelete.right;
            while (node.left != null) {
                node = node.left;
            }
            T value = toDelete.value;
            toDelete.value = node.value;
            node.value = value;
        }
        // now node definitely has 0 or 1 children
        RBInternalNode kid = node.left == null ? node.right : node.left;
        // detach
        if (node.parent == null) {
            this.root = kid;
        } else if (node.parent.left == node) {
            node.parent.left = kid;
        } else {
            node.parent.right = kid;
        }

        if (kid != null) {
            kid.parent = node.parent;
        }

        if (node.color == Color.BLACK) {
            if (kid != null && kid.color == Color.RED) {
                kid.color = Color.BLACK;
            } else {
                kid = new RBInternalNode(null, Color.DOUBLE_BLACK);
                kid.parent = node.parent;
                if (node.parent == null) {
                    this.root = kid;
                } else if (node.parent.left == null) {
                    node.parent.left = kid;
                } else {
                    node.parent.right = kid;
                }
                adjustDoubleBlack(kid);
                if (kid.parent == null) {
                    this.root = null;
                } else if (kid.parent.left == kid) {
                    kid.parent.left = null;
                } else {
                    kid.parent.right = null;
                }
                // gc hopefully takes care of kid
            }
        }
        System.gc();
    }

    private void adjustDoubleBlack(RBInternalNode toRemove) {
        RBInternalNode node = toRemove;
        while (true) {
            if (node.parent == null) {
                // node is root
                node.color = Color.BLACK;
                return;
            }
            RBInternalNode parent = node.parent;
            RBInternalNode sibling = node.getSibling();
            // black parent and red sibling
            if (sibling.color == Color.RED) {
                sibling.color = Color.BLACK;
                parent.color = Color.RED;
                if (node == parent.left) {
                    leftRotate(parent);
                    sibling = parent.right;
                } else {
                    rightRotate(parent);
                    sibling = parent.left;
                }
            }

            // black sibling, black children
            if (parent.color == Color.BLACK &&
                    (sibling.left == null || sibling.left.color == Color.BLACK) &&
                    (sibling.right == null || sibling.right.color == Color.BLACK)) {
                sibling.color = Color.RED;
                node = parent;
                continue;
            }

            // everybody black only the parent is red
            if (parent.color == Color.RED &&
                    (sibling.left == null || sibling.left.color == Color.BLACK) &&
                    (sibling.right == null || sibling.right.color == Color.BLACK)) {
                sibling.color = Color.RED;
                parent.color = Color.BLACK;
                return;
            }

            // black sibling, sibling's left child red
            if (node == parent.left &&
                    (sibling.right == null || sibling.right.color == Color.BLACK)) {
                sibling.color = Color.RED;
                sibling.left.color = Color.BLACK;
                rightRotate(sibling);
                sibling = sibling.parent;

                // black sibling, sibling's right child red
            } else if (node == parent.right &&
                    (sibling.left == null || sibling.left.color == Color.BLACK)) {
                sibling.color = Color.RED;
                sibling.right.color = Color.BLACK;
                leftRotate(sibling);
                sibling = sibling.parent;
            }

            sibling.color = parent.color;
            parent.color = Color.BLACK;

            if (node == parent.left) {
                leftRotate(parent);
                sibling.right.color = Color.BLACK;
            } else {
                rightRotate(parent);
                sibling.left.color = Color.BLACK;
            }
            return;
        }
    }

    @Override
    public void print() {
        this.printer.printTree(this.root);
    }

    enum Color {BLACK, DOUBLE_BLACK, RED}

    class RBInternalNode implements Comparable<RBInternalNode> {
        T value;
        RBInternalNode left;
        RBInternalNode right;
        RBInternalNode parent;
        Color color;

        RBInternalNode(T value) {
            this(value, Color.RED);
        }

        RBInternalNode(T value, Color color) {
            this.value = value;
            this.color = color;
        }

        @Override
        public int compareTo(RBInternalNode node) {
            return this.value.compareTo(node.value);
        }

        @Override
        public String toString() {
            return new StringBuilder().append(this.value.toString()).append(this.color == Color.RED ? "*" : "")
                    //.append(" (").append(parent == null ? "x" : parent.value.toString()).append(")")
                    .toString();
        }

        RBInternalNode getGrandparent() {
            return this.parent != null ? this.parent.parent : null;
        }

        RBInternalNode getUncle() {
            RBInternalNode grandpa = this.getGrandparent();
            if (grandpa == null) return null;
            return grandpa.left == this.parent ? grandpa.right : grandpa.left;
        }

        private RBInternalNode getSibling() {
            return this == parent.left ? parent.right : parent.left;
        }
    }
}
