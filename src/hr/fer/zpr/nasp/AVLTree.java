package hr.fer.zpr.nasp;

import lib.tech.vanyo.treePrinter.TreePrinter;

public class AVLTree<T extends Comparable<T>> implements BalancedTree<T> {
    @Override
    public BalancedTree delete(T value) {
        this.root = delete(this.root, value);
        return this;
    }

    private AVLInternalNode insert(AVLInternalNode root, AVLInternalNode newValue) {
        if (root == null) {
            return newValue;
        }
        if (newValue.compareTo(root) <= 0) {
            root.left = insert(root.left, newValue);
        } else {
            root.right = insert(root.right, newValue);
        }
        return renormalizeNode(root, newValue.value);
    }

    private AVLInternalNode renormalizeNode(AVLInternalNode root, T value) {
        root.calculateHeight();
        if (root.getBalanceFactor() < -1) {
            // there is definitely something on the left side, therefore root.left cannot be null whatsoever
            if (root.left != null && value.compareTo(root.left.value) <= 0) {
                return rightRotate(root);
            } else {
                return leftRightRotate(root);
            }
        } else if (root.getBalanceFactor() > 1) {
            if (root.right != null && value.compareTo(root.right.value) <= 0) {
                return rightLeftRotate(root);
            } else {
                return leftRotate(root);
            }
        }
        return root;
    }

    private AVLInternalNode leftRotate(AVLInternalNode root) {
        AVLInternalNode kid = root.right;
        root.right = kid.left;
        kid.left = root;
        root.calculateHeight();
        kid.calculateHeight();
        return kid;
    }

    private AVLInternalNode rightLeftRotate(AVLInternalNode root) {
        AVLInternalNode kid = root.right.left;
        root.right.left = kid.right;
        kid.right = root.right;
        root.right = kid.left;
        kid.left = root;
        kid.left.calculateHeight();
        kid.right.calculateHeight();
        kid.calculateHeight();
        return kid;
    }

    private AVLInternalNode leftRightRotate(AVLInternalNode root) {
        AVLInternalNode kid = root.left.right;
        root.left.right = kid.left;
        kid.left = root.left;
        root.left = kid.right;
        kid.right = root;
        kid.left.calculateHeight();
        kid.right.calculateHeight();
        kid.calculateHeight();
        return kid;
    }

    private AVLInternalNode rightRotate(AVLInternalNode root) {
        AVLInternalNode kid = root.left;
        root.left = kid.right;
        kid.right = root;
        root.calculateHeight();
        kid.calculateHeight();
        return kid;
    }

    private AVLInternalNode root = null;

    private TreePrinter<AVLInternalNode> printer = new TreePrinter<>(AVLInternalNode::toString, n -> n.left, n -> n.right);

    public AVLTree() {
    }

    @Override
    public BalancedTree insert(T value) {
        AVLInternalNode newValue = new AVLInternalNode(value);
        this.root = insert(this.root, newValue);
        return this;
    }

    private int height(AVLInternalNode node) {
        return node == null ? 0 : node.height;
    }

    private AVLInternalNode delete(AVLInternalNode root, T value) {
        AVLInternalNode temp;
        if (root == null) {
            return null;
        } else if (value.compareTo(root.value) < 0) {
            root.left = delete(root.left, value);
        } else if (value.compareTo(root.value) > 0) {
            root.right = delete(root.right, value);
        } else
            // OK SO HERE WE FIND THAT THE `root` SHOULD BE DELETED
            // we now delete it
            if (root.left != null && root.right != null) {
                AVLInternalNode smallerNode = root.left;
                AVLInternalNode biggerNode = root.right;
                temp = biggerNode;
                // find both the predecessor and successor and delete the deepest of them
                // if both are in the same depth, give priority to the successor
                while (smallerNode.right != null || biggerNode.left != null) {
                    if (smallerNode.right != null) {
                        smallerNode = smallerNode.right;
                        temp = smallerNode;
                    }
                    if (biggerNode.left != null) {
                        biggerNode = biggerNode.left;
                        temp = biggerNode;
                    }
                }
                root.value = temp.value;
                temp.value = value;
                // temp and root have switched their values. that means we have to change our search condition
                // for example, let us suppose, without the loss of generality, that we're to delete the successor
                // successor is a leftmost descendant of a right child
                // if we swap their values, the current node gets a bigger value, and the successor gets a smaller value
                // but, even though it has a bigger value, he came from a right subtree. that's the reason why this condition
                // is now inverted.
                if (temp.compareTo(root) >= 0) {
                    root.left = delete(root.left, value);
                } else {
                    root.right = delete(root.right, value);
                }
            } else {
                // else it has 0 or 1 child, that's easy
                if (root.left == null) root = root.right;
                else root = root.left;
                // GC takes care of the old root (hopefully)
        }

        if (root == null) return root; // we have changed it in the last if block
        root.calculateHeight();
        if (root.getBalanceFactor() < -1) {
            if (root.left.getBalanceFactor() == -1) {
                return rightRotate(root);
            } else {
                return leftRightRotate(root);
            }
        } else if (root.getBalanceFactor() > 1) {
            if (root.right.getBalanceFactor() == 1) {
//                System.out.println("HERE");
                return leftRotate(root);
            } else {
                return rightLeftRotate(root);
            }
        }
        return root;
    }

    @Override
    public void print() {
        printer.printTree(this.root);
        System.out.println();
    }

    class AVLInternalNode implements Comparable<AVLInternalNode> {
        T value;
        AVLInternalNode left = null;
        AVLInternalNode right = null;
        int height = 1;

        AVLInternalNode(T value) {
            this.value = value;
        }

        @Override
        public int compareTo(AVLInternalNode node) {
            return value.compareTo(node.value);
        }

        int getBalanceFactor() {
            return (right == null ? 0 : right.height) - (left == null ? 0 : left.height);
        }

        public String toString() {
            return new StringBuilder(value.toString())
//                    .append(" (D=").append(this.height).append(" F=").append(this.getBalanceFactor()).append(")")
                    .toString();
        }

        int calculateHeight() {
            this.height = 1 + Math.max(height(this.left), height(this.right));
            return this.height;
        }
    }
}