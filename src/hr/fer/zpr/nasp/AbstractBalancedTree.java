package hr.fer.zpr.nasp;

import lib.tech.vanyo.treePrinter.TreePrinter;

public abstract class AbstractBalancedTree<T extends Comparable<T>> implements BalancedTree <T> {
    protected abstract class InternalNode implements Comparable<InternalNode> {
        T value;
        InternalNode left;
        InternalNode right;

        InternalNode(T input) {
            this.value = input;
        }
        InternalNode() {this(null);}

        @Override
        public int compareTo(InternalNode other) {
            return value.compareTo(other.value);
        }
    }

    InternalNode root = null;
    private TreePrinter<InternalNode> printer;

    AbstractBalancedTree() {
        this.printer = new TreePrinter<>(n -> String.valueOf(n.value), n -> n.left, n -> n.right);
//        this.printer.setSquareBranches(true);
        this.printer.setHspace(5);
    }

    public void print() {
        printer.printTree(this.root);
    }

    AbstractBalancedTree rotateLeft(InternalNode node) {
        InternalNode parent = node.right;
        InternalNode child = node.right.right;
        parent.right = child.left;
        child.left = parent;
        node.right = child;
        return this;
    }
}
