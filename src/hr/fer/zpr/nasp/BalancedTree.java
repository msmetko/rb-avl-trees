package hr.fer.zpr.nasp;

public interface BalancedTree<T> {
    BalancedTree insert(T value);
    BalancedTree delete(T value);
    void print();
}
